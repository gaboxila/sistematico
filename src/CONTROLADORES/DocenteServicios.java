/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CONTROLADORES;

import MODELOS.ModeloDocente;
import OBJETOS.AREA;
import OBJETOS.Docente;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;

/**
 *
 * @author gabox
 */
public class DocenteServicios {

    ModeloDocente modelo;
    
    public DocenteServicios() {
        modelo = new ModeloDocente();
    }
    
    public DocenteServicios(ModeloDocente modelo){
        this.modelo = modelo;
    }
    
    public void guardar(Docente docente){
        modelo.agregar(docente);
    }
    
    public int editar(Docente docente){
        return modelo.editar(docente);
    }
    
    public void saveOrUpdate(Docente docente){
        Docente temporal = modelo.findAny((d)->d.getCodigo()== docente.getCodigo());
        if(temporal == null){
            modelo.agregar(docente);
        }else{
            modelo.editar(docente);
        }
    }
    
    public boolean eliminar(Docente docente){
        return modelo.eliminar(docente);
    }
    
    public ComboBoxModel getComboArea(){
        return new DefaultComboBoxModel(AREA.values());
    }
    
}
