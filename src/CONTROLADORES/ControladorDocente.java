package CONTROLADORES;

import MODELOS.ModeloDocente;
import OBJETOS.Docente;
import java.util.List;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

/**
 *
 * @author gabox
 */
public class ControladorDocente {

    private ModeloDocente modeloDocente;
    
    private String[] header = {
        "Nombre","Apellido","Cédula","Área","Sexo","Salario"
    };
    
    public ControladorDocente() {
        modeloDocente = new ModeloDocente();
    }
    
    public ControladorDocente(ModeloDocente modelo){
        this.modeloDocente = modeloDocente;
    }
    
    public ModeloDocente getModel(){
        return modeloDocente;
    }
    
    public String[] getHeader(){
        return header;
    }
    
    public Docente findDocenteByCodigo(int codigo){
        return modeloDocente.findAny((d)-> (d.getCodigo() == codigo));
    }
    
    public Object[][] getData(){
        return getData(modeloDocente.todos());
    }
    
    public Object[][] getData(List<Docente> lista){
        Object[][] data = null;
        
        if(lista.isEmpty()){
            return data;
        }
        
        data = new Object[lista.size()][header.length];
        
        int i = 0;
        
        for(Docente d: lista){
            data[i] = retornaString(d);
            i++;
        }
        
        return data;
    }
    
    public Object[] retornaString(Docente docente){
        Object[] fila = new Object[7];
        fila[0] = docente.getCodigo();
        fila[1] = docente.getNombre();
        fila[2] = docente.getApellido();
        fila[3] = docente.getCedula();
        fila[4] = docente.getArea();
        fila[5] = docente.getSexo();
        fila[6] = docente.getSalario();
        return fila;
    }
    
    public TableModel getTableModel(){
        return new DefaultTableModel(getData(),header);
    }
}
