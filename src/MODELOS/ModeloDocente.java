/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MODELOS;

import OBJETOS.Docente;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 *
 * @author gabox
 */
public class ModeloDocente {

    private List<Docente> docentes;
    
    public ModeloDocente() {
        docentes = new ArrayList<>();
    }
    
    public void agregar(Docente docente){
        docentes.add(docente);
    }
    
    public int editar(Docente docente){
        int indice = Collections.binarySearch(docentes, docente,
                (Docente t, Docente t1)->{
                    if(t.getCodigo()>t1.getCodigo()){
                        return 1;
                    }else if(t.getCodigo()<t1.getCodigo()){
                        return -1;
                    }else{
                        return 0;
                    }
                });
        
        if(indice < 0){
            return -1;
        }
        
        docentes.set(indice, docente);
        
        return 0;
    }
    
    public boolean eliminar(Docente docente){
        return docentes.remove(docente);
    }
    
    public List<Docente> todos(){
        return docentes;
    }
    
    public Docente findAny(Predicate<Docente> p){
        Optional<Docente> opt = docentes.stream().filter(p).findAny();
        return opt.isPresent() ? opt.get() : null;
    }
    
    public List<Docente> findMany(Predicate<Docente> p){
        return docentes.stream().filter(p).collect(Collectors.toList());
    }
}
