package OBJETOS;

/**
 *
 * @author gabox
 */
public class Docente {

    public Docente(){
        
    }
    
    public Docente(int codigo, String nombre, String apellido, String cedula, AREA Area, SEXO sexo, double salario) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.apellido = apellido;
        this.cedula = cedula;
        this.Area = Area;
        this.sexo = sexo;
        this.salario = salario;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }
    
    private int codigo;
    private String nombre;
    private String apellido;
    private String cedula;
    private AREA Area;
    private SEXO sexo;
    private double salario;

    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public String getCedula() {
        return cedula;
    }

    public AREA getArea() {
        return Area;
    }

    public SEXO getSexo() {
        return sexo;
    }

    public double getSalario() {
        return salario;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public void setArea(AREA Area) {
        this.Area = Area;
    }

    public void setSexo(SEXO sexo) {
        this.sexo = sexo;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }
    
    
}
